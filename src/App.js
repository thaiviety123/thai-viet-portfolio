import logo from './logo.svg';
import './App.css';
import { Routes, Route} from 'react-router-dom';
import HomePage from './components/HomePage';
import Navbar from './components/navbar';
function App() {
  return (
    <div className="App">
      <Navbar></Navbar>
      <Routes>
        <Route path="" element={<HomePage></HomePage>}></Route>
      </Routes>
    </div>
  );
}

export default App;
