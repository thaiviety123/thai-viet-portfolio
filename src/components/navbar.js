import "../styles/Navbar.css"
import logo from "../resources/logo.svg"
// const logo = 
export default function Navbar() {
    return (
        <div className="navbar-container">
            <div></div>
            <div className="navbar">
                <a id="logo" href="/"><img id="logo-svg" src={logo}></img></a>
                <a href='#introduction'>About me</a>
                <a href='/projects'>My projects</a>
                <input type="text"></input>
            </div>  
            <div></div>
        </div>
    )
}