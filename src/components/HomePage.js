import "../styles/HomePage.css"
import portrait from "../resources/portrait.png"
// import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import { useState } from "react"
const slash = <svg className="slash" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512"><path d="M432 256c0 17.7-14.3 32-32 32L48 288c-17.7 0-32-14.3-32-32s14.3-32 32-32l352 0c17.7 0 32 14.3 32 32z"/></svg>
const dot = <svg className="dot" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M256 512A256 256 0 1 0 256 0a256 256 0 1 0 0 512z"/></svg>
const down_pointer = <svg id="down-pointer" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512"><path d="M201.4 342.6c12.5 12.5 32.8 12.5 45.3 0l160-160c12.5-12.5 12.5-32.8 0-45.3s-32.8-12.5-45.3 0L224 274.7 86.6 137.4c-12.5-12.5-32.8-12.5-45.3 0s-12.5 32.8 0 45.3l160 160z"/></svg>
export default function HomePage() {
    const [extendOrCollapse, change] = useState(true )
    const changeState = () => {
        change(!extendOrCollapse)
        if (extendOrCollapse) {
            document.getElementById("timeline-row-wrap").style.height = "fit-content"
        }
        else
        document.getElementById("timeline-row-wrap").style.height = "500px"
        console.log('click')
    }
    return (
        <div className="home-page">
            <div className="header">
                <div className="title">
                    <div className="name">
                        <h1>Hi. I'm Thai Viet Y</h1>
                        <span>a.k.a cgthaiviet</span>
                    </div>
                    <div className="role">
                        <p>Graphic Designer / 3D Artist / Game Developer</p>
                    </div>
                </div>
                <div className="portrait">
                    <img src={portrait}></img>
                </div>
                <div className="scroll-down-button">
                    <a className="scroll-down-button" href="#introduction">Scroll down</a>
                    {down_pointer}
                </div>
            </div>
            <div className="content">
                <div className="content-header">
                    <div className="blank"></div>
                    <div className="content-header-wrapper">
                        {/* <div className="blank"></div> */}
                        <p>Passion</p>
                        <div className="slash-container">
                            {slash}
                        </div>
                        {/* <FontAwesomeIcon icon="fa-solid fa-minus" /> */}
                        {/* <div className="slash"></div> */}
                        <p>YOLO</p>
                        <div className="slash-container">
                            {slash}
                        </div>
                        {/* <FontAwesomeIcon icon="fa-solid fa-minus" /> */}
                        {/* <div className="slash"></div> */}
                        <p>Redamacy</p>
                        {/* <div className="blank"></div> */}
                    </div>
                    <div className="blank"></div>
                </div>
                <p>I live for passion, always look to the future but do not forget to live fully for the present. <br/> “You only life once” - Live to have no regrets.</p>
                <div className="content-body">
                    <div className="segment1" id="introduction">
                        <h3 className="title">Introduction</h3> <h3></h3>
                        <span>I'm currently reside in a small village in Quang Tri, Vietnam. Ever sine I was young, I have been utterly passionate about art and technology. From that time onward, I have always want to synchronize and combine my two passions.</span>
                        <span id='font-thin'>Realising the great potential of the <span id='red-span'>Game industry</span>, RMIT has become the spearhead to create Game Design majors - which currently is nowhere to be seen in most university in Vietnam. That said, I wish to have a chance to be recognized by RMIT through this portfolio, to let you know that I am choosing this path with certainty and determination.<br/><br/>Because to me, RMIT's Game Design majors is an ideal destination to make my dream come true. </span>
                    </div>
                    <div className="segment1">
                        <h3 className="title">About me</h3> <h3></h3>
                        <span>I’m Thai Viet Y, born in 26/04/2005.<br/><span id='gold-span'>cgthaiviet</span> is my stage name on platforms like Facebook, Behance, Tiktok,...I had more than 3 years experience in Design Industry as a Graphic Designer, Illustration Artist, 3D Artist and Game Developer. </span>
                        <span id='font-thin'>By the end of my secondary school years, which was the first ever COVID outbreak, I had more time to research and explore more possible paths. I have considered a variety of potential jobs and soon realize my talent for <span id='red-span'>Graphic Design</span>. I went on that path throughout my high school years.<br/><br/>In 11th grade, I attempted to learn <span id='red-span'>3D Design</span> and eventually fell in love with it.<br/><br/>As soon as I hit 12th grade, I combined my accumulated skills for 3D Art and newly acquired knowledge about <span id='red-span'>Game Developing</span> to create my own game has the content of <span id='red-span'>recreating Vietnam's historical sites on the digital environment</span> to bring value to the society.</span>
                    </div>
                    <div className="segment2-wrapper">
                        <h3 className="title" >Highlight</h3>
                        <div className="segment2">
                            <div className="blank"></div>
                            <div className="wrapper" grid-area='b'>
                                <h3><span id="gold-span">3rd Prize</span></h3>
                                <h3>ViSEF 2022</h3>
                            </div>
                            <div className="blank"></div>
                            <div className="wrapper"grid-area='d'>
                                <h3><span id="gold-span">1rd Prize</span></h3>
                                <h3>SV-STARTUP 2023</h3>
                            </div>
                            <div className="blank"></div>
                            <div className="wrapper"grid-area='f'>
                                <h3><span id="gold-span">4th Prize</span></h3>
                                <h3>ViSEF 2023</h3>
                            </div>
                            <div className="blank"></div>
                            <div className="wrapper"grid-area='h'>
                                <h3><span id="gold-span">85% Offer</span></h3>
                                <h3>Fullbright University</h3>
                            </div>
                            <div className="blank"></div>
                            <div className="wrapper"grid-area='j'>
                                <h3><span id="gold-span">3 Certificates</span></h3>
                                <h3>Adobe Certified Professional</h3>
                            </div>
                            <div className="blank"></div>
                            <div className="wrapper"grid-area='l'>
                                <h3><span id="gold-span">Top 4</span></h3>
                                <h3>HUTECH Uniform Design Competition 2021</h3>
                            </div>
                            <div className="blank"></div>
                            <div className="wrapper"grid-area='n'>
                                <h3><span id="gold-span">25.000+</span></h3>
                                <h3>Followers on Tiktok</h3>
                            </div>
                        </div>
                    </div>
                    <div className="segment1">
                        <h3 className="title">Timeline</h3><h3></h3>
                        <div id="timeline-row-wrap">
                            <div className="works">
                                <div className="divider">
                                    <span id="gold-span">2020</span>
                                    <div id='gold-line'></div>
                                </div>
                                <div className="2020">
                                    <span id='font-bold'>{dot} Le Quy Don High School for the Gifted - Informatics class</span>
                                    <div className="work-wrap">
                                        <span id="font-bold" className="work-title">{dot} Illustration/ Graphic Design Freelancer <span id="gray-span">(5/2020 - 12/2020)</span></span><br/>
                                        <div className="work-note">
                                            <span id="gray-span" >Worked mainly in Facebook through groups</span><br/>
                                            <span >Target : <span id="gray-span">Non-profit Organization, Brand, Med-budget,...</span></span><br/>
                                            <span >Typical : <span id="gray-span">Logo design, poster, banner, POD, brand identity,...</span></span><br/>
                                        </div>
                                    </div>

                                </div>
                                <div className="divider">
                                    <span id="gold-span">2021</span>
                                    <div id='gold-line'></div>
                                </div>
                                <div className="2021">

                                    <div className="work-wrap">
                                        <span id="font-bold" className="work-title">{dot} Graphic Design Freelancer  <span id="gray-span">(01/2021 - 08/2021)</span></span><br/>
                                        <div className="work-note">
                                            <span id="gray-span" >Worked mainly in Facebook through groups</span><br/>
                                            <span >Target : <span id="gray-span">Non-profit Organization, Brand, Med-budget,...</span></span><br/>
                                            <span >Typical : <span id="gray-span">Logo design, poster, banner, POD, brand identity,...</span></span><br/>
                                        </div>
                                    </div>
                                    <div className="work-wrap">
                                        <span id="font-bold" className="work-title">{dot} 4th Prize at HUTECH Uniform Design Competition 2021 <span id="gray-span">(03/2021)</span></span><br/>
                                        {/* <span id="gray-span" >Worked mainly in Facebook through groups</span><br/> */}
                                        <div className="work-note">
                                            <span >Award title : <span id="gray-span">Best Message Award</span></span><br/>
                                            <span >Typical : <span id="gray-span">3D design, 3D art, 3D modeling, individual art,...</span></span><br/>
                                        </div>
                                    </div>
                                    <div className="work-wrap">
                                        <span id="font-bold" className="work-title">{dot} 1st Prize at 3D Artwork Creative minigame by 3DF <span id="gray-span">(11/2021)</span></span><br/>
                                        {/* <span id="gray-span" >Worked mainly in Facebook through groups</span><br/> */}
                                        <div className="work-note">
                                            <span >With the artwork called :  <span id="gray-span">“Robot Phong Lon”</span></span><br/>
                                        </div>
                                        {/* <span >Typical : <span id="gray-span">3D design, 3D art, 3D modeling, individual art,...</span></span><br/> */}
                                    </div>
                                    <div className="work-wrap">
                                        <span id="font-bold" className="work-title">{dot} Adobe Certified Professional certificate  <span id="gray-span">(11/2021)</span></span><br/>
                                        <div className="work-note">
                                            <span id="gray-span" >Visual Design using Adobe Photoshop</span><br/>
                                        </div>
                                        {/* <span >With the artwork called :  <span id="gray-span">“Robot Phong Lon”</span></span><br/> */}
                                        {/* <span >Typical : <span id="gray-span">3D design, 3D art, 3D modeling, individual art,...</span></span><br/> */}
                                    </div>
                                </div>
                                <div className="divider">
                                    <span id="gold-span">2022</span>
                                    <div id='gold-line'></div>
                                </div>
                                <div className="2022">
                                    <div className="work-wrap">
                                        <span id="font-bold" className="work-title">{dot} Content Creator on Tiktok   <span id="gray-span">(01/2022)</span></span><br/>
                                        <div className="work-note">
                                            <span id="gray-span" >Username : @cgthaiviet   .   25.000+ Followers</span><br/>
                                            <span id="gray-span" >2D & 3D Design content</span><br/>
                                            <span id="gray-span" >More than 3 videos reached over 1 million views</span><br/>
                                            <span id="gray-span" >Highest views reached : 3,8 million / 231k likes</span><br/>
                                        </div>    
                                        {/* <span >Target : <span id="gray-span">Non-profit Organization, Brand, Med-budget,...</span></span><br/> */}
                                        {/* <span >Typical : <span id="gray-span">Logo design, poster, banner, POD, brand identity,...</span></span><br/> */}
                                    </div>
                                    <div className="work-wrap">
                                        <span id="font-bold" className="work-title">{dot} 1st Prize at Vietnam Science and Engineering Fair 2022 Provincial Level <span id="gray-span">(01/2022)</span></span><br/>
                                    </div>
                                    <div className="work-wrap">
                                        <span id="font-bold" className="work-title">{dot} 3rd Prize at Vietnam Science and Engineering Fair 2022 <span id="gray-span">(3/2022)</span></span><br/>
                                        {/* <span id="gray-span" >Visual Design using Adobe Photoshop</span><br/> */}
                                        <div className="work-note">
                                            <span >Project name : <span id="gray-span">Automated Robot Collects Samples For COVID-19 Testing</span></span><br/>
                                            <span >Field :  <span id="gray-span">Robotics and Intelligent Machines (ROBO)</span></span><br/>
                                            <span >Researchers : <span id="gray-span">Thai Viet Y (me) and Tran Quoc Hung </span></span><br/>
                                            <span >Execution Time : <span id="gray-span">08/2021 - 03/2022</span></span><br/>
                                            <span >Role : <span id="gray-span">Mechanic design, 3D design, simulation, optimize</span></span><br/>
                                        </div>
                                    </div>
                                    <div className="work-wrap">
                                        <span id="font-bold" className="work-title">{dot} Designer at “Road to Olympus” <span id="gray-span">(03/2022)</span></span><br/>
                                        <div className="work-note">
                                            <span id="gray-span" >A competition with a concept similar to “Road to Olympia” Organized by Le Quy Don High School for the Gifted</span><br/>
                                            <span id="gray-span" >Role : Logo Design, Poster Design, Social Post</span><br/>
                                        </div>
                                        {/* <span >Target : <span id="gray-span">Non-profit Organization, Brand, Med-budget,...</span></span><br/> */}
                                        {/* <span >Typical : <span id="gray-span">Logo design, poster, banner, POD, brand identity,...</span></span><br/> */}
                                    </div>
                                    <div className="work-wrap">
                                        <span id="font-bold" className="work-title">{dot} Adobe Certified Professional cerificate <span id="gray-span">(05/2022)</span></span><br/>
                                        <div className="work-note">
                                            <span id="gray-span" >Graphic Design & Illustration using Adobe Illusatrator</span><br/>
                                            {/* <span id="gray-span" >Role : Logo Design, Poster Design, Social Post</span><br/> */}
                                        </div>
                                        {/* <span >Target : <span id="gray-span">Non-profit Organization, Brand, Med-budget,...</span></span><br/> */}
                                        {/* <span >Typical : <span id="gray-span">Logo design, poster, banner, POD, brand identity,...</span></span><br/> */}
                                    </div>
                                    <div className="work-wrap">
                                        <span id="font-bold" className="work-title">{dot} Adobe Certified Professional cerificate <span id="gray-span">(05/2022)</span></span><br/>
                                        <div className="work-note">
                                            <span id="gray-span" >Visual Design using Adobe Photoshop</span><br/>
                                            {/* <span id="gray-span" >Role : Logo Design, Poster Design, Social Post</span><br/> */}
                                        </div>
                                        {/* <span >Target : <span id="gray-span">Non-profit Organization, Brand, Med-budget,...</span></span><br/> */}
                                        {/* <span >Typical : <span id="gray-span">Logo design, poster, banner, POD, brand identity,...</span></span><br/> */}
                                    </div>
                                    <div className="work-wrap">
                                        <span id="font-bold" className="work-title">{dot} 4th Prize at CiC - Creative Idea Challenge  <span id="gray-span">(10/2022)</span></span><br/>
                                        <div className="work-note">
                                            <span id="gray-span" >Project name : Bambloo</span><br/>
                                            <span id="gray-span" >Idea : Courses Market</span><br/>
                                            <span id="gray-span" >Role : UI Design, Graphic Design</span><br/>
                                            <span id="gray-span" >Execution Time :</span><br/>
                                            <span id="gray-span" >Number of member :</span><br/>
                                            {/* <span id="gray-span" >Role : Logo Design, Poster Design, Social Post</span><br/> */}
                                        </div>
                                        {/* <span >Target : <span id="gray-span">Non-profit Organization, Brand, Med-budget,...</span></span><br/> */}
                                        {/* <span >Typical : <span id="gray-span">Logo design, poster, banner, POD, brand identity,...</span></span><br/> */}
                                    </div>
                                    <div className="work-wrap">
                                        <span id="font-bold" className="work-title">{dot} Raise Funds Project <span id="gray-span">(11/2022)</span></span><br/>
                                        <div className="work-note">
                                            <span id="gray-span" >Project name : “Gây quỹ đến trường cùng em”</span><br/>
                                            <span id="gray-span" >Target : Middle and high school students in Dakrong, Quang Tri</span><br/>
                                            <span id="gray-span" >Role : Leader designer, logo design, social post, influencer</span><br/>
                                            <span id="gray-span" >Execution Time :</span><br/>
                                            <span id="gray-span" >Number of member :</span><br/>
                                            <span id="gray-span" >Total donation :</span><br/>
                                            {/* <span id="gray-span" >Role : Logo Design, Poster Design, Social Post</span><br/> */}
                                        </div>
                                        {/* <span >Target : <span id="gray-span">Non-profit Organization, Brand, Med-budget,...</span></span><br/> */}
                                        {/* <span >Typical : <span id="gray-span">Logo design, poster, banner, POD, brand identity,...</span></span><br/> */}
                                    </div>
                                    <div className="work-wrap">
                                        <span id="font-bold" className="work-title">{dot} Short-term Designer at Mixer  <span id="gray-span">(11/2022)</span></span><br/>
                                        <div className="work-note">
                                            <span id="gray-span" >A fashion brand created by Trang Nguyen, based in Hanoi</span><br/>
                                            <span id="gray-span" >Role : Designer</span><br/>
                                            <span id="gray-span" >Execution Time : 11/2022</span><br/>
                                                                                    {/* <span id="gray-span" >Role : Logo Design, Poster Design, Social Post</span><br/> */}
                                        </div>
                                        {/* <span >Target : <span id="gray-span">Non-profit Organization, Brand, Med-budget,...</span></span><br/> */}
                                        {/* <span >Typical : <span id="gray-span">Logo design, poster, banner, POD, brand identity,...</span></span><br/> */}
                                    </div>
                                    <div className="work-wrap">
                                        <span id="font-bold" className="work-title">{dot} Designer at Le Quy Don High School Startup Club  <span id="gray-span">(12/2022)</span></span><br/>
                                        
                                        {/* <span >Target : <span id="gray-span">Non-profit Organization, Brand, Med-budget,...</span></span><br/> */}
                                        {/* <span >Typical : <span id="gray-span">Logo design, poster, banner, POD, brand identity,...</span></span><br/> */}
                                    </div>
                                </div>
                                <div className="divider">
                                    <span id="gold-span">2023</span>
                                    <div id='gold-line'></div>
                                </div>
                                <div className="2023">
                                    <div className="work-wrap">
                                        <span id="font-bold" className="work-title">{dot} 2nd Prize at Vietnam Science and Engineering Fair 2023 Provincial Level  <span id="gray-span">(01/2023)</span></span><br/>
                                        
                                        {/* <span >Target : <span id="gray-span">Non-profit Organization, Brand, Med-budget,...</span></span><br/> */}
                                        {/* <span >Typical : <span id="gray-span">Logo design, poster, banner, POD, brand identity,...</span></span><br/> */}
                                    </div>
                                    <div className="work-wrap">
                                        <span id="font-bold" className="work-title">{dot} 3rd Prize at Vietnam Science and Engineering Fair 2022 <span id="gray-span">(3/2022)</span></span><br/>
                                        {/* <span id="gray-span" >Visual Design using Adobe Photoshop</span><br/> */}
                                        <div className="work-note">
                                            <span >Project name : <span id="gray-span">Building a Virtual reality model of a Special National Monument Quang Tri Ancient Citadel</span></span><br/>
                                            <span >Field :  <span id="gray-span">Systems Software (SOFT)</span></span><br/>
                                            <span >Researchers : <span id="gray-span">Thai Viet Y (me) and Nguyen Cao Minh Huyen </span></span><br/>
                                            <span >Execution Time : <span id="gray-span">08/2022 - 03/2023</span></span><br/>
                                            <span >Role : <span id="gray-span">Leader, Generate ideas, Design, 3D Modeling, Application Programming, Collect Database, System Optimization</span></span><br/>
                                        </div>
                                    </div>
                                    <div className="work-wrap">
                                        <span id="font-bold" className="work-title">{dot} 3rd Prize at Vietnam Science and Engineering Fair 2022 <span id="gray-span">(3/2022)</span></span><br/>
                                        {/* <span id="gray-span" >Visual Design using Adobe Photoshop</span><br/> */}
                                        <div className="work-note">
                                            <span >Project name : <span id="gray-span">Building a Virtual reality model of a Special National Monument Quang Tri Ancient Citadel</span></span><br/>
                                            <span >Field :  <span id="gray-span">Education and tourism</span></span><br/>
                                            {/* <span >Researchers : <span id="gray-span">Thai Viet Y (me) and Nguyen Cao Minh Huyen </span></span><br/> */}
                                            {/* <span >Execution Time : <span id="gray-span">08/2022 - 03/2023</span></span><br/> */}
                                            <span >Role : <span id="gray-span">Leader, Generate ideas, Design, 3D Modeling, Application Programming, Collect Database, System Optimization</span></span><br/>
                                            <span >Number of member :  <span id="gray-span">5</span></span><br/>
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="blank">
                            </div>
                        </div>
                        <div className="blank"/>
                        <button className="timeline-collapse-button" onClick={changeState}>{extendOrCollapse ? "extend" : "collapse"}</button>
                        {/* <div/> */}
                    </div>
                </div>
            </div>
        </div>
    )
}